#!/bin/bash
echo ""
echo "Portainer"
echo "Maintainer: Benoit Lavorata"
echo "License: MIT"
echo "==="
echo ""

FILE=".env"
if [ -f $FILE ]; then
    echo ""
    echo "File $FILE exists, will now start: ..."
    export $(cat .env | xargs)

    echo ""
    echo "Create networks ..."
    docker network create $PORTAINER_CONTAINER_NETWORK

    echo ""
    echo "Create volumes ..."
    docker volume create --name $PORTAINER_CONTAINER_VOLUME_DATA

    #echo ""
    #echo "Set portainer password file"
    #mkdir volumes
    #echo $PORTAINER_PASSWORD > volumes/portainer_password
    echo "Make sure to set up admin password"

    echo ""
    echo "Boot ..."
    docker compose up -d --remove-orphans $1
else
    echo "File $FILE does not exist: deploying for first time"
    cp .env.template .env
    
    echo -e "Make sure to modify .env according to your needs, then use ./up.sh again."
fi

